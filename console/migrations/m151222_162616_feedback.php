<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_162616_feedback extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subject}}', [
            'sid' => $this->primaryKey(),
            'title' => $this->string(60)->notNull(),
                ], $tableOptions);

        $this->createTable('{{%feedback}}', [
            'fid' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'body' => $this->text(),
            'email' => $this->string(60)->notNull(),
            'name' => $this->string(60)->notNull(),
            'file' => $this->string(255),
            'publish_date' => $this->integer()->notNull(),
                ], $tableOptions);

        $this->createIndex('FK_feedback_subject', '{{%feedback}}', 'subject_id');
        $this->addForeignKey(
                'FK_feedback_subject', '{{%feedback}}', 'subject_id', '{{%subject}}', 'sid', 'CASCADE'
        );
    }

    public function down() {
        $this->dropTable('{{%feedback}}');
        $this->dropTable('{{%subject}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
