<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'feedback' => 'feedback/feedback/create',
                'feedback/<id:\d+>' => 'feedback/feedback/view',
                'feedback/all' => 'feedback/feedback/index',
                '<modules>' => '<modules>/<controller>/index',
                '<modules>/<action>' => '<modules>/<controller>/<action>',
                '<modules>/<action>/<id:\d+>' => '<modules>/<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
                '<modules>/<controller>/<action>' => '<modules>/<controller>/<action>'
            ]
        ],
    ],
    'modules' => [
        'feedback' => [
            'class' => 'frontend\modules\feedback\Module',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];
}

return $config;
