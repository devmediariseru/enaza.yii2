<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=enaza',
            'username' => 'enaza',
            'password' => 'enaza',
            'charset' => 'utf8',
            'tablePrefix' => 'ez_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'dataCache' => [
            'class' => 'mediarise\MRCacheManager',
            'redis' => [
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0,
                'password' => 'nlwg43wd'
            ]
        ]
    ],
];
