<?php

namespace frontend\modules\feedback;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\feedback\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
