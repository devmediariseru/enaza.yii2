<?php

namespace frontend\modules\feedback\models;

use Yii;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property integer $fid
 * @property integer $subject_id
 * @property string $body
 * @property string $email
 * @property string $name
 * @property string $file
 * @property integer $publish_date
 */
class Feedback extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['subject_id', 'email', 'name', 'publish_date'], 'required'],
            [['subject_id', 'publish_date'], 'integer'],
            [['body'], 'string'],
            [['email', 'name'], 'string', 'max' => 60],
            [['file'], 'string', 'max' => 255],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'fid' => 'Fid',
            'subject_id' => 'Тема',
            'body' => 'Текст',
            'email' => 'Email',
            'name' => 'Ваше имя',
            'file' => 'Прикрепить файл',
            'publish_date' => 'Дата публикации',
        ];
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery the active query used by this AR class.
     */
    public static function find() {
        return new FeedbackQuery(get_called_class());
    }

}
