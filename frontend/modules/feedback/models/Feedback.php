<?php

namespace frontend\modules\feedback\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/*
 * This is the model class for table "{{%feedback}}".
 *
 * @property integer $fid
 * @property integer $subject_id
 * @property string $body
 * @property string $email
 * @property string $name
 * @property string $file
 */

class Feedback extends ActiveRecord {

    public $fileUpload;
    public $verifyCode;
    public $fileSize;

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'publish_date',
                ],
                'value' => function() {
            return date('U');
        },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['subject_id', 'email', 'name'], 'required'],
            [['subject_id'], 'integer'],
            [['body'], 'string'],
            [['email', 'name'], 'string', 'max' => 60],
            ['email', 'email'],
            [['file'], 'string', 'max' => 255],
            [['fileUpload'], 'file', 'extensions' => 'png, jpg, pdf', 'maxSize' => 2 * (1024 * 1024)],
            ['verifyCode', 'captcha', 'captchaAction' => 'feedback/feedback/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'fid' => 'Fid',
            'subject_id' => 'Тема',
            'body' => 'Текст',
            'email' => 'Email',
            'name' => 'Ваше имя',
            'file' => 'Прикрепить файл',
            'fileUpload' => 'Загрузить файл',
            'verifyCode' => 'Введите код капчи',
            'publish_date' => 'Дата публикации',
            'fileSize' => 'Размер файла'
        ];
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery the active query used by this AR class.
     */
    public static function find() {
        return new FeedbackQuery(get_called_class());
    }

    /**
     * fetch stored image file name with complete path 
     * @return string
     */
    public function getFileUpload() {
        return isset($this->file) ? Yii::getAlias('@webroot') . Yii::$app->params['uploadPath'] . $this->file : null;
    }

    /**
     * fetch stored image url
     * @return string
     */
    public function getFileUrl() {
        // return a default image placeholder if your source image is not found
        $fileUpload = isset($this->file) ? $this->file : '';
        return Yii::getAlias('@web') . Yii::$app->params['uploadPath'] . $fileUpload;
    }

    /**
     * Process upload of image to directory /frontend/web/uploads
     *
     * @return mixed the uploaded image instance
     */
    public function uploadFile() {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $fileUpload = UploadedFile::getInstance($this, 'fileUpload');

        // if no image was uploaded abort the upload
        if (empty($fileUpload)) {
            return false;
        }

        // store the source file name
        $ext = $fileUpload->extension;
        $this->fileSize = round($fileUpload->size / 1024);
        // generate a unique file name
        $this->file = Yii::$app->security->generateRandomString() . ".{$ext}" . " Размер: {$this->fileSize} КБ";

        // the uploaded image instance
        return $fileUpload;
    }

    /**
     * Process deletion of image
     *
     * @return boolean the status of deletion
     */
    public function deleteFile() {
        $file = $this->getFileUpload();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->file = null;

        return true;
    }

    public function getSubject() {
        return $this->hasOne(Subject::className(), ['sid' => 'subject_id']);
    }

    public function sendEmail($email) {
        $subject = Subject::find()->where(['sid' => $this->subject_id])->one();
        return Yii::$app->mailer->compose()
                        ->setTo($email)
                        ->setFrom([$this->email => $this->name])
                        ->setSubject($subject->title)
                        ->setTextBody($this->body)
                        ->send();
    }

}
