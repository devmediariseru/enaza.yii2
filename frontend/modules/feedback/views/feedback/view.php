<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\feedback\models\Feedback */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fid',
            'subject.title',
            'body:ntext',
            'email:email',
            'name',
            [
                'format' => 'raw',
                'attribute' => 'file',
                'value' => ($model->file !== null) ? Html::a($model->file, [Yii::$app->params["uploadPath"] . $model->file], []) . ' ' . $model->fileSize : 'Нет прикрепленных файлов',
            ],
            [
                'attribute' => 'publish_date',
                'value' => date('Y-m-d H:i:s', $model->publish_date)
            ]
        ],
    ])
    ?>

</div>
